package com.example.pietrogirardi.eventbus;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.example.pietrogirardi.eventbus.eventbus.MessageEB;

import de.greenrobot.event.EventBus;

/**
 * Created by pietrogirardi on 02/11/15.
 */
public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        // EventBus register
        EventBus.getDefault().registerSticky(SecondActivity.this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(SecondActivity.this);
        Log.i("LOG", "SecondActivity - onDestroy()");
    }

    public void onEventMainThread(MessageEB mMessageEB){
        Log.i("LOG", "SecondActivity - onEventMainThread()");

        if(mMessageEB.getList() != null){
            Toast.makeText(SecondActivity.this,
                    "Name: " + mMessageEB.getList().get(0).getName() + "\nJob: " + mMessageEB.getList().get(0).getJob(),
                    Toast.LENGTH_LONG).show();
        }
    }
}
