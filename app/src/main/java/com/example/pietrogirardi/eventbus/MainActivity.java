package com.example.pietrogirardi.eventbus;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.pietrogirardi.eventbus.eventbus.MessageEB;
import com.example.pietrogirardi.eventbus.fragments.FragmentBottom;
import com.example.pietrogirardi.eventbus.fragments.FragmentTop;
import com.example.pietrogirardi.eventbus.services.ServiceTest;

import de.greenrobot.event.EventBus;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("LOG", "MainActivity onCreate() 1");

        EventBus.getDefault().register(MainActivity.this);

        Intent intent = new Intent(MainActivity.this, ServiceTest.class);
        startService(intent);

        FragmentTop fragmentTop = new FragmentTop();
        FragmentBottom fragmentBottom = new FragmentBottom();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.llContainerFragmentTop, fragmentTop);
        transaction.replace(R.id.llContainerFragmentBottom, fragmentBottom);
        transaction.commit();

    }


    public void askAboutPerson(View view){
        MessageEB messageEB = new MessageEB();
        messageEB.setClassTester(ServiceTest.class + "");

        EventBus.getDefault().post(messageEB);

        Log.i("LOG", "MainActivity AskAboutPerson() - POST");
    }

    public void callSecondActivity(View view){
        Intent intent = new Intent(MainActivity.this, SecondActivity.class);
        startActivity(intent);
    }

    public void onEventMainThread(MessageEB mMessageEB){
        Log.i("LOG", "MainActivity - onEventMainThread()");

        if(!mMessageEB.getClassTester().equalsIgnoreCase(MainActivity.class+""))
            return;

        if(mMessageEB.getList() != null){
            Toast.makeText(MainActivity.this,
                    "Name: " + mMessageEB.getList().get(0).getName() + "\nJob: " + mMessageEB.getList().get(0).getJob(),
                    Toast.LENGTH_LONG).show();
        }
    }

    public void onEvent(MessageEB messageEB){
        Log.i("LOG", "MainActivity.this.onEvent()");

        if(!messageEB.getClassTester().equalsIgnoreCase(MainActivity.class+""))
            return;

        if(messageEB.getNumber() >= 0){
            Log.i("LOG", "MainActivity EVENT - number = "+messageEB.getNumber());
        }
        if(messageEB.getText() != null){
            Log.i("LOG", "MainActivity EVENT - text = "+messageEB.getText());
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(MainActivity.this);
        Log.i("LOG", "MainActivity onDestroy()");
    }
}
